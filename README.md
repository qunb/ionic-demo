# 1.init project

`npm install`       
`cordova platform add android`<br>
`cordova platform add ios`

# 2.build project
`ionic cordova build android`<br>
`ionic cordova build ios`
# 3.run project
`ionic cordova run android`<br>
`ionic cordova run ios`
  

# For capacitor：

### 1.cd pos-ionic-demo

### 2.run command:

```
npm install @capacitor/core @capacitor/cli

npm install @capacitor/android
```



##### Tip：

You can directly run the project without any command（use IDE to open the project）；

And if you have met the error like：

- [x] `Cannot read property 'dspread_pos_plugin' of undefined`

Try： 

```
npm install  D:\gitlab\ionic\pos-ionic-demo\plugins\posPlugin (our plugin directory)
```

*D:\gitlab\ionic\pos-ionic-demo\plugins\posPlugin*  is our plugin directory		

​		or 

​		1）delete the andorid file

​		2）re-add it 

```
npx cap add android

ionic build

npx cap copy
```

- [x]  `import io.ionic.starter.R didn't exist`

Try  use 

```
import capacitor.android.plugins.R
```

 instand of 

```
import io.ionic.starter.R
```

in dspread_pos_plugin.java

