import { Component } from '@angular/core';

declare let cordova : any;
 /**
     * Transaction type.
     */
enum TransactionType {
      GOODS, // 货物 GOODS
      SERVICES, // 服务 service
      CASH, // 现金 cash
      CASHBACK, //  返现
      INQUIRY, // 查询
      TRANSFER, // 转账
      ADMIN, // 管理
      CASHDEPOSIT, // 存款
      PAYMENT, // 付款 支付

      PBOCLOG, // 0x0A /*PBOC日志(电子现金日志)*/
      SALE, // 0x0B /*消费*/
      PREAUTH, // 0x0C /*预授权*/

      ECQ_DESIGNATED_LOAD, // 0x10 /*电子现金Q指定账户圈存*/
      ECQ_UNDESIGNATED_LOAD, // 0x11 /*电子现金费非指定账户圈存*/
      ECQ_CASH_LOAD, // 0x12 /*电子现金费现金圈存*/
      ECQ_CASH_LOAD_VOID, // 0x13 /*电子现金圈存撤销*/
      ECQ_INQUIRE_LOG, // 0x0A /*电子现金日志(和PBOC日志一样)*/
      REFUND,//退款
      UPDATE_PIN,     //修改密码
      SALES_NEW,
      NON_LEGACY_MONEY_ADD, /* 0x17*/
      LEGACY_MONEY_ADD  /*0x16*/
}

enum CardTradeMode {
  ONLY_INSERT_CARD, 
  ONLY_SWIPE_CARD,
  TAP_INSERT_CARD, 
  TAP_INSERT_CARD_NOTUP,
  SWIPE_TAP_INSERT_CARD, 
  UNALLOWED_LOW_TRADE,
  SWIPE_INSERT_CARD, 
  SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE,
  SWIPE_TAP_INSERT_CARD_NOTUP_UNALLOWED_LOW_TRADE, 
  ONLY_TAP_CARD,
  ONLY_TAP_CARD_QF, 
  SWIPE_TAP_INSERT_CARD_NOTUP,
  SWIPE_TAP_INSERT_CARD_DOWN, 
  SWIPE_INSERT_CARD_UNALLOWED_LOW_TRADE,
  SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE_NEW,
  ONLY_INSERT_CARD_NOPIN
}

 /**
     * The different check value for different key type.
     */
enum CHECKVALUE_KEYTYPE {
      /**
       * TMK of MKSK
       */
      MKSK_TMK,
      /**
       * PIK of MKSK
       */
      MKSK_PIK,
      /**
       * TDK of MKSK
       */
      MKSK_TDK,
      /**
       * MCK of MKSK
       */
      MKSK_MCK,
      /**
       * TCK
       */
      TCK,
      /**
       * MAGK
       */
      MAGK,
      /**
       * TRK TPEK of DUKPT
       */
      DUKPT_TRK_IPEK,
      /**
       * EMV IPEK of DUKPT
       */
      DUKPT_EMV_IPEK,
      /**
       * PIN IPEK of DUKPT
       */
      DUKPT_PIN_IPEK,
      /**
       * TRK KSN of DUKPT
       */
      DUKPT_TRK_KSN,
      /**
       * EMV KSN of DUKPT
       */
      DUKPT_EMV_KSN,
      /**
       * PIN KSN of DUKPT
       */
      DUKPT_PIN_KSN,
      /**
       * ALL TYPE of DUKPT and MKSK
       */
      DUKPT_MKSK_ALLTYPE
  }

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage{
  constructor() {
     console.log("init....");
  }

 scanQPos2Mode() : void{
    console.log("scanQPos2Mode");
    var tabled = document.getElementById("tablediv");
    var txtresult = <HTMLInputElement>document.getElementById("posResult");
    tabled.style.display='block';
    txtresult.style.display='none';
    cordova.plugins.dspread_pos_plugin.scanQPos2Mode((success : any) =>{
              console.log("scanQPos2Mode->success: " + success);
              this.addTorow(success);
          },(fail : any) =>{
              console.log("scanQPos2Mode->fail: " + fail);
              this.posresult(fail);
          },30);
 }

 dotrade() {
    console.log("dotrade");
    // cordova.plugins.dspread_pos_plugin.setCardTradeMode((success : any) =>{
    //   console.log("setCardTradeMode->success: " + success);
    // },(fail : any) =>{
    //   console.log("setCardTradeMode->fail: " + fail);
    // },CardTradeMode.ONLY_SWIPE_CARD);

    cordova.plugins.dspread_pos_plugin.doTrade((success : string) =>{
      console.log("dotrade->success: " + success);
      this.posresult(success);
      if(success == "onRequestSetAmount"){
        this.inputAmount();
      }else if(success.indexOf("onRequestOnlineProcess") == 0){
        this.sendOnlineResult("8A023030");
      }
  },(fail : any) =>{
      console.log("dotrade->fail: " + fail);
      this.posresult(fail);
  },0,20);
 }


 sendOnlineResult(tlv : string) {
  console.log("sendOnlineResult");
  cordova.plugins.dspread_pos_plugin.sendOnlineProcessResult((success : any) =>{
    console.log("sendOnlineProcessResult->success: " + success);
  },(fail : any) =>{
    console.log("sendOnlineProcessResult->fail: " + fail);
  },tlv);
}

getKeyCheckValue(keyIndex : any, keyType : CHECKVALUE_KEYTYPE ) {
  console.log("getKeyCheckValue");
  cordova.plugins.dspread_pos_plugin.getKeyCheckValue((success : any) =>{
    console.log("getKeyCheckValue->success: " + success);
  },(fail : any) =>{
    console.log("getKeyCheckValue->fail: " + fail);
  },keyIndex, keyType);
}

 disConnectBT() {
    console.log("disConnectBT");
    cordova.plugins.dspread_pos_plugin.disconnectBT((success : any) =>{
      console.log("disConnectBT->success: " + success);
      this.posresult(success);
  },(fail : any) =>{
      console.log("disConnectBT->fail: " + fail);
      this.posresult(fail);
  });
 }


 updateEMVConfigureByXml(){
  this.posresult("start update emv configure, pls wait...");
  var rawFile = new XMLHttpRequest();
  var readResult = false;
  var allText;
  rawFile.open("GET", 'emv_profile_tlv.xml', false);
  rawFile.onreadystatechange = function ()
  {
    if(rawFile.readyState === 4)
    {
      if(rawFile.status === 200 || rawFile.status == 0)
      {
        allText = rawFile.responseText;
        console.log("success:allText " +allText);
        readResult = true;
      }
    } 
  }
  rawFile.send(null);
  if(readResult){
    cordova.plugins.dspread_pos_plugin.updateEMVConfigByXml((success : any) =>{
      this.posresult(success);
      console.log("success: " +success);
    },(fail : any) =>{
      console.log("fail: " +fail);
      this.posresult(fail);
    },allText);
  }
}
 
 addTorow(str : string){ 
  var tbody = document.querySelector('tbody');
  var tr = document.createElement('tr');
  var td1 = document.createElement('td');
  td1.innerHTML = str.split(' ')[0];
  tr.append(td1);
  tbody.append(tr);
  tr.onclick= ()=>{//on click of select
      console.log("click " + str);
      this.posresult("bluetooth connecting...");
      cordova.plugins.dspread_pos_plugin.connectBluetoothDevice((success : any) =>{
         console.log("connectBluetoothDevice->success"+success);
         this.posresult(success);
      },(fail : any) =>{
         console.log("connectBluetoothDevice->fail"+fail);
         this.posresult(fail);
      },true, str);
   };
}

 posresult(text : string){//display the pos status
  var tbody = document.querySelector('tbody');
  var tabled = document.getElementById("tablediv");
  var txtresult = <HTMLInputElement>document.getElementById("posResult");
  txtresult.style.display="none";
  tabled.style.display='none';
  txtresult.style.display='block';
  txtresult.value = text;
}

inputAmount(){
    console.log("inputAmount");
    var confirmtd=prompt("please input amount","12")
    console.log("TDsuccess:"+confirmtd);
    if(confirmtd){
      cordova.plugins.dspread_pos_plugin.setAmount((success : any) =>{
            console.log("scanQPos2Mode->success: " + success);
            this.posresult(success);
        },(fail : any) =>{
            console.log("scanQPos2Mode->fail: " + fail);
            this.posresult(fail);
        },confirmtd,0,"0156",TransactionType.GOODS);
    }
}

onReturnCustomConfigResult(str : string){
  this.posresult(str);
}

}
